import React from "react";
import ListMovie from "./ListMovie/ListMovie";
import Banner from "../../Component/Banner/Banner";
import Event from "./Event/Event";
import TabMove from "./TabMovie/TabMove";
import Popup from "./PopupBanner/Popup";
import AppMpbile from "./Mobile/AppMpbile";
import { Footer } from "antd/es/layout/layout";
import FooterMovie from "../../Component/Footer/FooterMovie";
import Header from "../../Component/Header/Header";
import { Desktop } from "../../Layouts/Responsive";
export default function HomePage() {
  return (
    <div>
      <Banner />
      <ListMovie />
      <Event />
      <Desktop>
        <TabMove />
      </Desktop>
      <AppMpbile />
    </div>
  );
}
