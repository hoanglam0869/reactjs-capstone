import React, { useEffect, useState } from "react";
import { movieService } from "../../../services/movieService";
import CardMovie from "../../../Pages/HomePage/ListMovie/CardMovie.js";

export default function ListMovie() {
  const [movie, Setmovie] = useState();
  useEffect(() => {
    movieService
      .getMovieList()
      .then((res) => {
        // console.log("res: ", res);
        Setmovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []); // khi dùng useEffect nhớ truyền tham số thứ 2 dependency []
  return (
    <div>
      <p className="mt-5 text-3xl">PHIM ĐANG CHIẾU</p>
      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 container">
        {movie?.map((item) => {
          return <CardMovie movie={item} />;
        })}
      </div>
    </div>
  );
}
