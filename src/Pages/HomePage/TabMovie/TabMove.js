import React, { useEffect, useState } from "react";
import { movieService } from "../../../services/movieService";
import { Tabs } from "antd";
import ChildTabMovie from "./ChildTabMovie";

export default function TabMove() {
  let [dataMovie, SetdataMovie] = useState([]);
  useEffect(() => {
    movieService
      .getMovieByTheater()
      .then((res) => {
        console.log(res);
        SetdataMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderHeThongRap = () => {
    return dataMovie.map((heThongRap, index) => {
      return {
        key: index,
        label: <img src={heThongRap.logo} className="w-16" alt="logo" />,
        children: (
          <Tabs
            style={{ height: 550 }}
            tabPosition="left"
            items={heThongRap.lstCumRap.map((cumRap) => {
              return {
                key: cumRap.maCumRap,
                label: <p>{cumRap.tenCumRap}</p>,
                children: <ChildTabMovie listMovie={cumRap.danhSachPhim} />,
              };
            })}
          />
        ),
      };
    });
  };
  return (
    <div className="">
      <p className="text-3xl my-10">HỆ THỐNG RẠP</p>
      <div className="container flex justify-center">
        <Tabs
          tabPosition="left"
          defaultActiveKey="1"
          items={renderHeThongRap()}
        />
      </div>
    </div>
  );
}
