import React, { useEffect, useState } from "react";
import { movieService } from "../../services/movieService";
import "./checkout.css";
export default function CheckOut() {
  const cssghe = "";
  const [seats, Setseats] = useState();
  useEffect(() => {
    movieService
      .QuanLyDatVe()
      .then((res) => {
        console.log(res);
        Setseats(res.data.content.danhSachGhe);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderGhe = () => {
    return seats?.map((ghe, index) => {
      let classGheVip = ghe.loaiGhe === "Vip" ? "gheVip" : "";
      let classGheDaDat = ghe.daDat === true ? "gheDaDat" : "";

      return (
        <button className={`ghe ${classGheVip} ${classGheDaDat}`} key={index}>
          <span>{ghe.stt}</span>
        </button>
      );
    });
  };
  return (
    <div className="container">
      <div className="grid grid-cols-12">
        <div className="col-span-8 ">
          <div className="trapezoid "></div>
          <div className="grid grid-cols-10 mt-5 space-y-2">{renderGhe()}</div>
        </div>
        <div className="col-span-4 text-left space-y-5">
          <h3 className="text-center text-3xl text-blue-500">0 VND</h3>
          <hr />
          <h3 className=" text-3xl my-5 text-center">Peaky Blinders</h3>
          <div className="flex justify-between">
            <p>Cụm Rạp:</p>
            <p className="text-blue-500">BHD Star Cineplex - 3/2</p>
          </div>
          <hr />
          <div className="flex justify-between">
            <p>Địa chỉ:</p>
            <p className="text-blue-500">L5-Vincom 3/2, 3C Đường 3/2, Q.10</p>
          </div>
          <hr />
          <div className="flex justify-between">
            <p>Rạp:</p>
            <p className="text-blue-500">Rạp 1</p>
          </div>
          <hr />
          <div className="flex justify-between">
            <p>Ngày giờ chiếu:</p>
            <p className="text-blue-500">30/07/2022 -11:07</p>
          </div>
          <hr />
          <div className="flex justify-between">
            <p>Tên Phim:</p>
            <p className="text-blue-500">PeaKy Blinders</p>
          </div>
          <hr />
          <div className="flex justify-between">
            <span className="text-red-500 text-xl">Ghế</span>
            <span className="text-blue-500">0</span>
          </div>
          <hr />

          <div className="my-10 text-white bg-blue-500 flex justify-center py-5 cursor-pointer">
            Đặt Vé
          </div>
        </div>
      </div>
    </div>
  );
}
