import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { movieService } from "../../services/movieService";
import detail from "../../assets/img/detail/movie-details-bg.jpg";
import { Progress, Space } from "antd";
export default function DetailPage() {
  let params = useParams();
  const [movie, setMovie] = useState({});
  useEffect(() => {
    movieService
      .getDetailMovie(params.id)
      .then((res) => {
        console.log(res);
        setMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div className="" style={{ backgroundImage: `url(${detail})` }}>
      <div className="container grid grid-cols-2 py-5">
        <div>
          <img
            src={movie.hinhAnh}
            className="w-auto mx-auto"
            style={{ width: 500, height: 600, objectFit: "fill" }}
            alt=""
          />
        </div>
        <div className="space-y-5 pl-10">
          <h2 className="text-white text-3xl mt-5">{movie.tenPhim}</h2>
          <p className="text-gray-200 text-xl">{movie.moTa}</p>
          <div className="flex items-center justify-center ">
            <Progress
              className="mt-10"
              type="circle"
              percent={movie.danhGia * 10}
              format={(persent) => {
                return persent / 10 + " Điểm";
              }}
            />
          </div>
        </div>
      </div>
    </div>
  );
}
