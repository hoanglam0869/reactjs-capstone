import { localUserService } from "../../services/localService";
import { USER_LOGIN } from "../constant/userConstant";

const initialState = {
  userInfor: localUserService.get(),
};

let userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case USER_LOGIN:
      //   state.userInfor = payload;
      return { ...state, userInfor: payload };

    default:
      return state;
  }
};

export default userReducer;
