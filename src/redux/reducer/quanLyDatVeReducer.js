const stateDefault = {
  chiTietPhongVe: {},
};

const QuanLyDatVeReducer = (state = stateDefault, action) => {
  switch (action.type) {
    default:
      return { ...state };
  }
};

export default QuanLyDatVeReducer;
